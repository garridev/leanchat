import * as React from 'react';

interface IBubbleMessageProps {
  role: string,
}

class BubbleMessage extends React.Component<IBubbleMessageProps, {}> {
  constructor(props: IBubbleMessageProps){
    super(props);
  }

  public render() {
    const classNames = "card "
    return (
      <div className={(this.props.role === "host") ? classNames + "bubble-message-host" : classNames + "bubble-message-guest"}>
        <span>{this.props.children}</span>
      </div>
    )
  }
}

export default BubbleMessage;