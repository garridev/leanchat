import * as React from 'react';

interface IHeaderProps {
  theme: string,
}

interface IHeaderState {
  typing: boolean,
}

class Header extends React.Component<IHeaderProps, IHeaderState> {
  constructor(props: IHeaderProps){
    super(props);
  }

  public render(){
    return (
      <div className="header">
        <div className="row">
          <div className="col-6 text-center">
            <img src="/imgs/chino.png" className="img-responsive" width="100" height="100" />
          </div>
          <div className="col-6">
            <span className="headline">John Smith <span className="badge badge-pill badge-success">Active</span></span>
            <span className="company-position">Customer Success Manager</span>
          </div>
        </div>
      </div>
    );
  }

}

export default Header;