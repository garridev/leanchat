import * as React from 'react';
import Header from '../components/Header';
import Body from '../containers/Body';
import Footer from '../containers/Footer';

interface IChatProps {
  theme: string,
}

class Chat extends React.Component<IChatProps, {}> {
  constructor(props: IChatProps){
    super(props);
  }

  public render(){
    return (
      <div className="chat card animated fadeIn">
        <Header theme={this.props.theme} />
        <Body backgroundColor="#f7f7f7"/>
        <Footer theme={this.props.theme} />
      </div>
    );
  }

}

export default Chat;