import * as React from 'react';
import { IMessage } from 'src/state/types';

interface IBubblePopProps{
  toogleChat: () => void,
  readChat: () => void,
  isChatVisible: boolean,
  notifications: boolean,
  messages: IMessage[],
}

interface IBubblePopState {
  typing: boolean,
  notifications: boolean,
}

class BubblePop extends React.Component<IBubblePopProps, IBubblePopState> {
  constructor(props: IBubblePopProps){
    super(props);
    this.state = { notifications: true, typing: false }
  }

  public toggleChat = () => {
    if(!this.props.isChatVisible) {
      this.props.readChat()
    }
    this.props.toogleChat();
  }

  public render(){
    return (
      <div className="bubble-pop-container">
         { this.props.notifications && 
          <div className="bubble-pop-message card animated fadeIn">
            <span>{this.props.messages[0].message}</span>
          </div>
        }
        <div id="bubble-pop" className="bubble-pop animated wobble" onClick={this.toggleChat}>
        { this.props.notifications && <span className="badge badge-pill badge-danger">1</span>}
        <span />
      </div>
      </div>
    );
  }

}

export default BubblePop;