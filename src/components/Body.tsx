import * as React from 'react';
import BubbleMessage from './BubbleMessage';

interface IBodyProps {
  backgroundColor: string,
  messages: Array<{
    role: string,
    message: string,
  }>,
}

class Body extends React.Component<IBodyProps, {}> {

  public bodyRef:React.RefObject<HTMLDivElement>;

  constructor(props: IBodyProps){
    super(props);

    this.bodyRef = React.createRef() // create a ref object
  }

  public componentDidMount(){
    this.autoScroll();
  }

  public componentDidUpdate() {
    this.autoScroll();
  }

  public autoScroll = () => {
    if (this.bodyRef.current !== null ) {
      this.bodyRef.current.scrollTop = this.bodyRef.current.scrollHeight
    }
  }

  public render(){
    return (
      <div ref={this.bodyRef} style={{ backgroundColor: this.props.backgroundColor }} className="body">
      {this.props.messages.length > 0 && 
        this.props.messages.map((message, i) => <BubbleMessage key={i} role={message.role}>{message.message}</BubbleMessage>)
      }
      </div>
    );
  }

}

export default Body;