import * as React from 'react';

interface IFooterProps {
  theme: string;
  sendMessage: (value: string) => void;
}

class Footer extends React.Component<IFooterProps, {}> {

  public inputRef: React.RefObject<HTMLInputElement>;

  constructor(props: IFooterProps){
    super(props);

    this.inputRef = React.createRef() // create a ref object
  }

  public handleOnClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    // send message
    event.preventDefault();
    if(this.inputRef.current && this.inputRef.current.value.trim()){
      this.props.sendMessage(this.inputRef.current.value)
      this.clearInput();
    }
  }

  public handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.charCode === 13){
      event.preventDefault();
      if(this.inputRef.current && this.inputRef.current.value.trim()){
        this.props.sendMessage(this.inputRef.current.value)
        this.clearInput();
      }
    }
  }

  public clearInput() {
    if( this.inputRef.current !== null){
      this.inputRef.current.value ='';
      this.inputRef.current.focus();
    }
  }

  public render(){
    return (
      <div className="footer">
        <form className="">
          <div className="input-group">
            <input ref={this.inputRef} type="text" className="form-control" id="message" placeholder="Type a message..." onKeyPress={this.handleKeyPress} />
            <div className="input-group-append">
              <button className="btn btn-outline-secondary" type="button" id="button-addon2" onClick={this.handleOnClick}>Send</button>
            </div>
          </div>      
        </form>
      </div>
    );
  }

}

export default Footer;