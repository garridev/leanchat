import { createStore } from 'redux';
import { IGlobalState, initialState as ie} from './state/globalState';
import reducer from './state/reducer';


export default (initialState: IGlobalState = ie) => {
  return createStore(reducer, initialState);
}