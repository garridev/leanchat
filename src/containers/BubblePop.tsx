import BubblePop from '../components/BubblePop'
import { connect } from 'react-redux';
import { IGlobalState } from '../state/globalState'
import { Dispatch } from 'redux';
import { READ_CHAT } from '../state/actions'

const mapStateToProps = (state: IGlobalState) => {
  return {
    messages: state.messages,
    notifications: state.notifications,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    readChat: () => {
      dispatch({ type: READ_CHAT })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BubblePop)