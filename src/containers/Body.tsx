import Body from '../components/Body'
import { connect } from 'react-redux';
import { IGlobalState } from '../state/globalState'

const mapStateToProps = (state: IGlobalState) => {
  return {
    messages: state.messages
  }
}

export default connect(mapStateToProps)(Body)