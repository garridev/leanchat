import Footer from '../components/Footer'
import { connect } from 'react-redux';
import { IGlobalState } from '../state/globalState'
import { Dispatch } from 'redux';
import { SEND_MESSAGE } from '../state/actions';

const mapStateToProps = (state: IGlobalState) => {
  return {}
}

const mapDispatchToProps = (dispatch: Dispatch ) => {
  return {
    sendMessage: (value: string) => {
      dispatch({type: SEND_MESSAGE, payload: value})
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Footer)