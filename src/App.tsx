import * as React from 'react';
import "./assets/css/main.css";
import BubblePop from './containers/BubblePop';
import Chat from './components/Chat';

interface IAppProps {
  theme: string,
}

interface IAppState {
  isChatVisible: boolean,
}

class App extends React.Component<IAppProps, IAppState> {
  
  constructor(props: IAppProps){
    super(props);
    this.state = {
      isChatVisible: false,
    }
  }

  public toogleChat = () =>{
    this.setState({isChatVisible: !this.state.isChatVisible})
  }

  public render() {
    return (
      <div>
        {this.state.isChatVisible && <Chat theme={this.props.theme} />}
        <BubblePop toogleChat={this.toogleChat} isChatVisible={this.state.isChatVisible}/>
      </div>
    );
  }
}

export default App;
