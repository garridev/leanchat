import { initialState, IGlobalState } from './globalState';
import { Action } from 'redux';
import { SEND_MESSAGE, READ_CHAT, ISendMessageAction } from './actions';

export default (state: IGlobalState = initialState, action: Action) => {
  switch(action.type){
    case SEND_MESSAGE:
      const sendMessageAction = action as ISendMessageAction;
      return {
        ...state,
        messages: [
          ...state.messages,
          {role: 'guest', message: sendMessageAction.payload, timestamp: new Date()},
        ],
      }
    case READ_CHAT:
      return {
        ...state,
        notifications: false,
      }
  }
  return state;
}