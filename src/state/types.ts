
export interface IMessage {
  message: string,
  timestamp: Date,
  role: MessageRole
}

export enum MessageRole {
  HOST = 'host',
  GUEST = 'guest'
}