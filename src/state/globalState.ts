import { IMessage, MessageRole } from './types';

export interface IGlobalState {
  messages: IMessage[],
  notifications: boolean,
}

export const initialState: IGlobalState = {
  messages: [{
    message: "Hi, Could I help you?",
    role: MessageRole.HOST,
    timestamp: new Date(),
  }],
  notifications: true,
}
