import { Action } from 'redux';


export const SEND_MESSAGE = "SEND_MESSAGE";
export const READ_CHAT = "READ_CHAT";

export interface ISendMessageAction extends Action {
  payload: string,
}